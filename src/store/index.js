import Vue from 'vue'
import Vuex from 'vuex'
import favorite from './modules/favorite'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    favorite,
  },
  strict: debug
})