const state = {
  favorite: []
}

const getters = {
  getCount (state){
    return state.favorite
  }
 }

const actions = {
  addFavorite: ({ commit }, payload) => {
    commit('ADD_FAVOURITE',payload)
  }
}

const mutations = {
  ADD_FAVOURITE (state, payload) {
    state.favorite.push(payload)

  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}