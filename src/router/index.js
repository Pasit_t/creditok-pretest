import Vue from 'vue'
import Router from 'vue-router'
import Favorite from '@/components/Favorite'
import BookShelf from '@/components/BookShelf'
import Info from '@/components/Info'


Vue.use(Router)

export default new Router({
  mode: 'history',
  duplicateNavigationPolicy: 'ignore',
  routes: [
    {
      path: '/favorite',
      name: 'Favorite',
      component: Favorite
    },
    {
      path: '/',
      name: 'BookShelf',
      component: BookShelf
    },
    {
      path: '/info',
      name: 'Info',
      component: Info
    }
  ]
})
